
# kde-service-menu-reimage

Service Menu on KDE for image manipulation with Reimage

--------------------------
<!-- TOC -->
Table des matières
- [Advances](#advances)
- [Description](#description)
	- [Functionalities](#functionalities)
	- [Metadata](#metadata)
	- [Tools](#tools)
- [ToDo](#todo)
<!-- /TOC -->

## [∞](#advances) Advances
This repository is a fork of [Giuseppe Benigno's](https://www.egregorion.net/ "see his personal web site") kde-service-menu-reimage project.
The actual version is ***2.6.0***

## [∞](#description) Description

Manipulate ***images*** and their ***metadata***. \
**kde-service-menu-reimage**{style="color:#7cb7ff"} is a package that extends the functionalities of *Dolphin/Konqueror*{style="color:orange"} adding many additionally sensible menu, reachables with right click on the files. It's similar to gnome nautilus actions.

!!! note Note
    These actions are related to picture files.
 
 ### [∞](#functionalities) Functionalities
 - Compress and resize
 - Advanced optimization for web
 - Compress in % (change quality)
 - Resize in % or square \
 <br/> 
 - Convert and rotate
 - Convert to all formats supported by ImageMagick
 - Convert to PDF or PDF/A-1
 - Generate favicons for browser/android/apple/ms
 - Convert to Base64
 - Generate favicons
 - Rotate
 - Overturn vertically/horizontally
 
 ### [∞](#metadata) Metadata
- Rename jpg and tiff files with data content in Exif metadata.
 (For example: 001.jpg -> 2018-04-27_133741.jpg)
- Rename jpg and tiff files with file's data.
- Set file's datetime from Exif date.
- Set file date from file's name
- Set Exif datetime from file's date.
- Set Exif datetime from file's name.
- Add comment
- View metadata
- Extract metadata to file
- Delete comment field
- Strip Exif section
- Delete IPTC section
- Delete XMP section
- Strip all unnecessary data
- Add timestamp from Exif

### [∞](#tools) Tools
   - Create animated GIF
   - Append to right
   - GrayScale
   - Sepia filter
   - Change transparent to color
   - Add colored border
   - Add transparent border
   - Drop shadow
 
 ## [∞](#todo) ToDo
 - [ ] *Adding others language translations*{style="margin-left:10px"}
 - [ ] *See and be inspired by the following projects:*{style="margin-left:10px"}
		- [ ] [KDE Servicemenus ImageTools by Marco Mania](https://github.com/marco-mania/kde-servicemenus-imagetools)
		- [ ] [KDE Service menu ReImage by BigLinux](https://github.com/biglinux/kde-service-menu-reimage)
		
 - [ ] *Contact the initiator of this project.* [Giuseppe Benigno](https://www.egregorion.net/)

 Copyright © 2018-2023 Giuseppe Benigno <giuseppe.benigno@gmail.com>
<!--  
::::: container
:::: row
::: col-sm-auto alert alert-success
success text
:::
::: col-sm-3 alert alert-warning
warning text
:::
::: col-sm-auto alert alert-danger
danger text
:::
::: col-xs-6 alert alert-primary
primary text
:::
::: col-sm-auto alert alert-secondary
secondary text
:::
::: col-xs-6 alert alert-light
light text
:::
::: col-xs-6 alert alert-dark
dark text
:::
::: col-xs-6 alert alert-heading
heading text
:::
::: col-sm-3 alert alert-link
link text
:::
::: col-sm-3 alert alert-heading
heading text
:::
::: col-sm-auto alert alert-dismissible
dismissible text
:::
::: col-xs-3 alert alert-heading badge-warning
heading text
:::
::: col-xs-3 alert alert-info
info text
:::
::::
:::::

item **bold red**{style="color:red"}

[[Ctrl+Esc]]

29^th^, H~2~O

Apple
:   Pomaceous fruit of plants of the genus Malus in the family Rosaceae.

Here is a footnote reference,[^1] and another.[^longnote]

[^1]: Here is the footnote.
[^longnote]: Here's one with multiple blocks.

*[HTML]: Hyper Text Markup Language
*[W3C]:  World Wide Web Consortium
The HTML specification
is maintained by the W3C.

!!! note

    This is the **note** admonition body

    !!! danger Danger Title
        This is the **danger** admonition body -->
